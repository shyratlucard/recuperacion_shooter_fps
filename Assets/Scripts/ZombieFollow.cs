﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieFollow : MonoBehaviour {

    public GameObject ThePlayer;
    public float TargetDistance;
    public float AllowedRange = 10;
    public GameObject TheEnemy;
    public float EnemySpeed;
    public int AttackTrigger;
    public RaycastHit Shot;

    //public int IsAttacking;
    public GameObject ScreenFlash;
    //public int PainSound;

    private void Update()
    {
        if (TheEnemy != null)
        {
            transform.LookAt(ThePlayer.transform);
            if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out Shot))
            {
                TargetDistance = Shot.distance;
                if (TargetDistance < AllowedRange)
                {
                    //Debug.LogWarning("TargetDistance < AllowedRange");
                    EnemySpeed = 0.01f;
                    if (AttackTrigger == 0)
                    {
                        //Debug.LogWarning("AttackTrigger == 0");
                        transform.position = Vector3.MoveTowards(transform.position, ThePlayer.transform.position, EnemySpeed);
                    }
                }
                else
                {
                    //Debug.LogError("else: TargetDistance < AllowedRange");
                    EnemySpeed = 0;
                }

                float dist = Vector3.Distance(ThePlayer.transform.position, TheEnemy.transform.position);
                //Debug.LogWarning("Distance to other: " + dist);
                if (dist < 2)
                {
                    StartCoroutine(EnemyDamage());
                    Destroy(TheEnemy);
                    //Destroy(gameObject);
                }
            }
        }        


        //if (AttackTrigger == 1)
        //{
        //    Debug.LogWarning("DENTRO!");
        //    if (IsAttacking == 0)
        //    {
        //        Debug.LogWarning("DENTRO! - if");
        //        StartCoroutine(EnemyDamage());
        //    }
        //    EnemySpeed = 0;
        //    //TheEnemy.GetComponent<Animation>().Play("attack");
        //}
    }



    //private void OnTriggerExit(Collider other)
    //{
    //    Debug.LogWarning("DENTRO: OnTriggerExit!");
    //    AttackTrigger = 0;
    //}

    IEnumerator EnemyDamage()
    {
        //IsAttacking = 1;
        yield return new WaitForSeconds(0.1f);
        ScreenFlash.SetActive(true);
        GlobalHealth.PlayerHealth -= 1;
        yield return new WaitForSeconds(0.2f);
        ScreenFlash.SetActive(false);
        Destroy(gameObject);
        //IsAttacking = 0;
    }
}
