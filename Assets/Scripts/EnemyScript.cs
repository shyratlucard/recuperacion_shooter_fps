﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour {

    public int EnemyHealth = 15;

	// Use this for initialization
	void Start () {
		
	}

    public void DeductPoints(int DamageAmount)
    {
        EnemyHealth -= DamageAmount;
    }
	
	// Update is called once per frame
	void Update () {
		if(EnemyHealth <= 0)
        {
            Destroy(gameObject);
        }
	}

    //private void OnTriggerEnter(Collider col)
    //{
    //    Debug.LogWarning("DENTRO: OnTriggerEnter!");
    //    //AttackTrigger = 1;
    //    if (col.gameObject.name == "FPSController")
    //    {
    //        GlobalHealth.PlayerHealth -= 1;
    //    }
    //}
}
